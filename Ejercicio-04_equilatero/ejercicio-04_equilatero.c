#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
    int lado = 0;
    int perimetro = 0.0f;

    printf("Ingrese el valor de cualquier lado: ");
    scanf("%d",&lado);

    perimetro = 3*lado;

    printf("EL perimetro es: %d",perimetro);

    return 0;
}
