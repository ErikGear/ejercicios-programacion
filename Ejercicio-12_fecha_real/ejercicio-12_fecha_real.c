#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int anio = 0;
    int mes = 0;
    int dia = 0;
    
    /*
    Tienen 31 días: Enero, marzo, mayo, julio, agosto, octubre y diciembre.

    Tienen 30 días: Abril, junio, septiembre y noviembre.

    Tienen 28 días: Febrero.
    */

    printf("Ingrese un anio: ");
    scanf("%d",&anio);

    printf("Ingrese un mes: ");
    scanf("%d",&mes);

    printf("Ingrese un dia: ");
    scanf("%d",&dia);

    if(mes <= 12){

        if (mes == 1 || mes == 3 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
        {
            if (dia>31)
            {
                printf("numero de dia invalido");
            } else {
                if (anio >0 && anio <=2025)
                {
                    printf("fecha correta: %d / %d / %d",dia,mes,anio);
                }
                
            }
            

        } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
        {
            if (dia>30)
            {
                printf("numero de dia invalido");
            } else {
                if (anio >0 && anio <=2025)
                {
                    printf("fecha correta: %d / %d / %d",dia,mes,anio);
                }
                
            }
        } else 
        {
            if (dia>28)
            {
                printf("numero de dia invalido");
            } else {
                if (anio >0 && anio <=2025)
                {
                    printf("fecha correta: %d / %d / %d",dia,mes,anio);
                }
                
            }
        }
        
    } else 
    {
        printf("Formato de fecha incorrecto");
    }
    
    

    return 0;
}
