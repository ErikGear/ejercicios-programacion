#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    for (int i = 1; i <= 100; i++)
    {
        if (i % 3 == 0)
        {
            printf("ping");
        } else if (i % 5 == 0)
        {
            printf("pong");
        } else if (i % 3 == 0 && i % 5 == 0)
        {
            printf("ping-pong");
        }
        
    }
    
    return 0;
}
