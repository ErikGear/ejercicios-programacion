#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    
    int a = 0;
    int b = 0;
    int perimetro = 0;

    printf("Ingrese el valor de un lado lateral:");
    scanf("%d",&a);
    
    printf("Ingrese el valor de la base:");
    scanf("%d",&b);

    perimetro = (2*a)+ b;

    printf("El perimetro corresponde a: %d",perimetro);

    return 0;
}
