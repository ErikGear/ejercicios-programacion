#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int numero = 0;

    do
    {
        printf("Ingrese un numero (0 - 11): ");
        scanf("%d",&numero);
    } while (numero < 0 || numero >11);
    
    switch (numero)
    {
    case 0:
        printf("Enero");
        break;
    case 1:
        printf("Febrero");
        break;
    case 2:
        printf("Marzo");
        break;
    case 3:
        printf("Abril");
        break;
    case 4:
        printf("Mayo");
        break;
    case 5:
        printf("Junio");
        break;
    case 6:
        printf("Julio");
        break;
    case 7:
        printf("Agosto");
        break;
    case 8:
        printf("Septiembre");
        break;
    case 9:
        printf("Octubre");
        break;
    case 10:
        printf("Noviembre");
        break;
    case 11:
        printf("Diciembre");
        break;
    default:
        printf("sin mes correspondiente");
        break;
    }


    return 0;
}
