#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    float dolares = 0.0f;
    float pesosMXN = 0.0f;

    printf("Sistema de converion de DOLARES  a PESOS MEXICANOS\n\n");

    printf("Ingrese la cantidad en dolares: ");
    scanf("%f",&dolares);

    //realizando la conversion de dolares a pesos tomando en cuenta la cotizacion del dia de hoy 16/10/2021

    pesosMXN = dolares * 20.34;

    printf("La conversion a pesos MXN es: %f",pesosMXN);

    return 0;
}
