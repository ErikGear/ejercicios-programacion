#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int anio;

    printf("Introduzca un anio: ");
    scanf( "%d", &anio );

    if ( anio % 4 == 0 && anio % 100 != 0 || anio % 400 == 0 )
        printf("es un anio biciesto");
    else
        printf("no es un anio biciesto");
    return 0;
}
